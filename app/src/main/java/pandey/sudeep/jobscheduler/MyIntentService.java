package pandey.sudeep.jobscheduler;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

//import static android.support.v4.app.NotificationManagerCompat.IMPORTANCE_HIGH;
import static android.app.NotificationManager.IMPORTANCE_HIGH;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {

    private static final String TAG = MyIntentService.class.getSimpleName();
    private int update = 0;

    private static String CHANNEL_ID = "1";
    NotificationChannel channel;
    private Context context = this;

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        for (int i=0;i<20;i++){
            //if(Thread.interrupted()) {
               // Log.i(TAG, "Thread interrupted: "+Thread.currentThread().getName()+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                //return;
            //}
            try {
                Thread.sleep(1000);
                update+=1;
                Log.i(TAG, "Service running in Thread: "+Thread.currentThread().getName()+" . Current Value: "+update+"................................................");
            } catch (InterruptedException e) {

            }

        }
        final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Create the NotificationChannel, but only on API 26+ because
            //the NotificationChannel class is new and not in the support library
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            channel = new NotificationChannel(CHANNEL_ID, name, IMPORTANCE_HIGH);
            channel.setDescription(description);
            // Register the channel with the system
            manager.createNotificationChannel(channel);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                    .setContentTitle("Notifying U")
                    .setContentText("Okay")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            manager.notify(1, mBuilder.build());

        }

        }
    @Override
    public void onDestroy() {

        super.onDestroy();

        Log.i(TAG, "Service destroyed in Thread: "+Thread.currentThread().getName()+"................................................");
    }


}
