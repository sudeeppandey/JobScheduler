package pandey.sudeep.jobscheduler;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.annotation.UiThread;
import android.util.Log;

public class MyJobService extends JobService {

    private static final String TAG = MyJobService.class.getSimpleName();
    private MyHandlerThread myHandlerThread;
    //private Handler uiHandler;
    private Runnable runnable;
    private Handler handler;

    private JobScheduler tm;
    private JobInfo.Builder builder;
    private ComponentName mServiceComponent;
    int update=0;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created in Thread: "+Thread.currentThread().getName()+"..................................................");
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        myHandlerThread.quit();
        myHandlerThread.interrupt();
        Log.i(TAG, "Service destroyed in Thread: "+Thread.currentThread().getName()+"................................................");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        //super.onStartCommand(intent,flags,startId);
        Log.i(TAG, "Service started in Thread: "+Thread.currentThread().getName()+"................................................");
        mServiceComponent = new ComponentName(this, MyJobService.class);
        builder = new JobInfo.Builder(1,mServiceComponent);
        builder.setMinimumLatency(10000);
        //builder.setOverrideDeadline(10000);
        //builder.setRequiresCharging(true);
        //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);//requires WiFi
        Log.d(TAG, "Scheduling job in thread: "+Thread.currentThread().getName());
        tm = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        tm.schedule(builder.build());
        Log.d(TAG, "Scheduled job in thread: "+Thread.currentThread().getName());
        return START_STICKY;
    }

    @Override
    public boolean onStartJob(final JobParameters params) {

        Log.i(TAG, "Job started in Thread: "+Thread.currentThread().getName()+"................................................");
        myHandlerThread = new MyHandlerThread("myHandlerThread");
       //uiHandler = new Handler(Looper.getMainLooper());
        //handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {

                for (int i=0;i<30;i++){
                    if(Thread.interrupted()) {
                        Log.i(TAG, "Thread interrupted: "+Thread.currentThread().getName()+"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
                        return;
                    }
                    try {
                        Thread.sleep(1000);
                        update+=1;
                        Log.i(TAG, "Service running in Thread: "+Thread.currentThread().getName()+" . Current Value: "+update+"................................................");
                    } catch (InterruptedException e) {

                    }

                }
                jobFinished(params,false);
                Log.i(TAG, "Job finished in Thread: "+Thread.currentThread().getName()+"................................................");

            }
       };
        //handler.post(runnable);

        myHandlerThread.start();
        myHandlerThread.prepareHandler();
        myHandlerThread.postTask(runnable);

                return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        myHandlerThread.quit();
        myHandlerThread.interrupt();
        //myHandlerThread=null;
        Log.i(TAG, "Job stopped in Thread: "+Thread.currentThread().getName()+"................................................");
        //do the work may be clean up of all unfinished task.

        return false;//will drop the job.
    }

    public class MyHandlerThread extends HandlerThread {

        private Handler handler;

        public MyHandlerThread(String name) {
            super(name);
            Log.i(TAG, "Creating handler thread "+Thread.currentThread().getName()+"................................................");

        }

        public void postTask(Runnable task) {
            handler.post(task);
        }

        public void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }
}
