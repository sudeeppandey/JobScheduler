package pandey.sudeep.jobscheduler;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ComponentName mServiceComponent;
    private JobScheduler tm;
    private JobInfo.Builder builder;

    private MyHandlerThread myHandlerThread;
    private Runnable runnable;

    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mServiceComponent = new ComponentName(this, MyJobService.class);

        //myHandlerThread = new MyHandlerThread("myHandlerThread");
        //runnable=new Runnable() {
            //@Override
            //public void run() {


            //}

       //};
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*builder = new JobInfo.Builder(1,mServiceComponent);
                builder.setMinimumLatency(10000);
                //builder.setOverrideDeadline(10000);
                //builder.setRequiresCharging(true);
                //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
                builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);//requires WiFi
                Log.d(TAG, "Scheduling job in thread: "+Thread.currentThread().getName());
                tm = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
                tm.schedule(builder.build());
                Log.d(TAG, "Scheduled job in thread: "+Thread.currentThread().getName());
                */
                startService(new Intent(context,MyIntentService.class));

            }
        });
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "Activity destroyed: "+Thread.currentThread().getName());
    }

    public class MyHandlerThread extends HandlerThread {

        private Handler handler;

        public MyHandlerThread(String name) {
            super(name);
        }

        public void postTask(Runnable task) {
            handler.post(task);
        }

        public void prepareHandler() {
            handler = new Handler(getLooper());
        }
    }
}
